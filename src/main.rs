mod commands;
mod config;
mod posts;
use crate::commands::post::daily_post;
use crate::config::get_config;
use commands::post::REDDIT_COMMAND;
use commands::help::HELP_COMMAND;
use commands::stats::STATS_COMMAND;
use config::Config;
use log::info;
use serenity::async_trait;
use serenity::framework::standard::macros::group;
use serenity::framework::standard::StandardFramework;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::model::id::GuildId;
use serenity::prelude::*;
use std::sync::atomic::{AtomicBool, Ordering};
use std::time::Duration;

#[group]
#[commands(reddit, help, stats)]
struct General;

struct Handler {
    loop_running: AtomicBool,
    //posted_daily: AtomicBool,
    //last_checked_day: NaiveDate
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, _ctx: Context, _msg: Message) {}
    async fn ready(&self, _ctx: Context, ready: Ready) {
        info!("{} is ready!", ready.user.name);
    }
    async fn cache_ready(&self, ctx: Context, _guilds: Vec<GuildId>) {
        info!("Built cache");

        if self.loop_running.load(Ordering::Relaxed) {
            return;
        }

        tokio::spawn(async move {
            loop {
                daily_post(&ctx).await.expect("Daily post");
                tokio::time::sleep(Duration::from_secs(60)).await;
            }
        });
        info!("Spawned a worker thread for daily posts");

        self.loop_running.swap(true, Ordering::Relaxed);
    }
}

static PREFIX: &str = "\\";
lazy_static::lazy_static! {
    pub static ref CONFIG: Config = get_config();
}

#[tokio::main]
async fn main() {
    std::env::set_var("RUST_LOG", "none,toru=info");
    log4rs::init_file("logging_config.yaml", Default::default()).unwrap();

    let framework = StandardFramework::new()
        .configure(|c| c.prefix(PREFIX))
        .group(&GENERAL_GROUP);
    info!("Configured framework");

    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(CONFIG.token(), intents)
        .event_handler(Handler {
            loop_running: AtomicBool::new(false),
            //posted_daily: AtomicBool::new(false),
            //last_checked_day: Local::now().date_naive()
        })
        .framework(framework)
        .await
        .expect("Error creating client");
    info!("Constructed client");

    // start listening for events by starting a single shard
    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {why:?}");
    }
}
