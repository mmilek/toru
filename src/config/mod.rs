use std::collections::HashMap;

use chrono::NaiveTime;
use log::info;
use reqwest::Url;
use serde::Deserialize;
use serenity::model::id::ChannelId;

#[derive(Deserialize, Clone)]
pub struct ConfigTmp {
    /// the Discord app token
    pub token: String,
    // where Toru will paste playlists shortcut commands
    pub music_channel: u64,
    /// Toru will save every message sent to channels with listed IDs and keep them in a JSON file
    pub archived_channels: Vec<u64>,
    /// Pairs of names and links
    pub playlists: Vec<(String, String)>,
    // vector of channels to which the daily posts should go
    pub daily_post_channel: u64,
    pub daily_post_hour: NaiveTime,
}

pub struct Config {
    pub token: String,
    pub music_channel: ChannelId,
    pub archived_channels: Vec<ChannelId>,
    pub playlist_map: HashMap<String, Url>,
    pub daily_post_channel: ChannelId,
    pub daily_post_hour: NaiveTime,
}

impl Config {
    fn from_tmp(value: ConfigTmp) -> Self {
        let music_channel = ChannelId::from(value.music_channel);

        let archives = value
            .archived_channels
            .iter()
            .map(|id| ChannelId::from(*id))
            .collect();

        let mut playlist_map = HashMap::<String, Url>::new();
        for (name, url) in value.playlists.iter() {
            playlist_map.insert(name.to_string(), Url::parse(url).expect("Parse URL"));
        }

        Config {
            token: value.token.clone(),
            music_channel,
            archived_channels: archives,
            playlist_map,
            daily_post_channel: ChannelId::from(value.daily_post_channel),
            daily_post_hour: value.daily_post_hour,
        }
    }

    pub fn token(&self) -> &str {
        self.token.as_ref()
    }
}

pub fn get_config() -> Config {
    let config = Config::from_tmp(
        serde_json::from_str(include_str!("../../config.json")).expect("serialize config.json"),
    );
    info!("Parsed config.json");
    config
}
