#[derive(Debug, Clone)]
pub struct RedditPost<'a> {
    //title: &'a str,
    //subreddit_name: &'a str,
    url: &'a str,
    //is_video: bool,
}

impl<'a> From<&'a serde_json::Value> for RedditPost<'a> {
    /// The value passed here should be the one you can get by going all the way down to the `data` property of a child (post) from Reddit API.
    fn from(value: &'a serde_json::Value) -> Self {
        RedditPost {
            //title: value["title"].as_str().unwrap(),
            //subreddit_name: value["subreddit_name_prefixed"].as_str().unwrap(),
            url: value["url"].as_str().unwrap(),
            //is_video: value["is_video"].as_bool().unwrap(),
        }
    }
}

impl RedditPost<'_> {
    pub fn url(&self) -> &str {
        self.url
    }
    pub fn is_image(&self) -> bool {
        let extension = self
            .url
            .rsplit_terminator('.')
            .take(1)
            .collect::<Vec<&str>>()[0];
        matches!(extension, "jpg" | "png")
    }
}
