use log::info;
use serenity::framework::standard::macros::command;
use serenity::framework::standard::CommandResult;
use serenity::model::channel::Message;
use serenity::prelude::*;
use serenity::utils::Color;

#[command]
#[aliases("s")]
async fn stats(ctx: &Context, msg: &Message) -> CommandResult {
    info!(
        "Use of STATS command by {}: {:?}",
        msg.author.name, msg.content
    );

    let guild = msg.guild(&ctx.cache).expect("Get guild");
    let channel_count = guild.channels.len();
    let member_count = guild.member_count;
    let icon_url = guild.icon_url();
    let emoji_count = guild.emojis.len();
    let bot_count = guild.members(&ctx.http, None, None).await?.iter().filter(|m| m.user.bot).count();
    let role_count = guild.roles.values().len();
    
    info!("Obtained all info for the STATS command");
    
    msg.channel_id
        .send_message(&ctx.http, |f| {
            f.embed(|e| {
                e.title(format!("{}'s Stats", guild.name))
                    .author(|a| {
                        a.name("Toru")
                            .icon_url("https://i.redd.it/4riuxto19rq01.jpg")
                    })
                    .fields(vec![
                        ("Member Count", member_count.to_string(), true),
                        ("Channel Count", channel_count.to_string(), true),
                        ("Role Count", role_count.to_string(), true),
                        ("Bot Count", bot_count.to_string(), true),
                        ("Emoji Count", emoji_count.to_string(), true),
                    ])
                    .image(icon_url.expect("Get icon URL"))
                    .color(Color::from_rgb(250, 206, 131))
            })
        })
        .await?;
    
    info!("Sent result of STATS command");
    
    Ok(())
}
