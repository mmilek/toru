use log::info;
use serenity::builder::CreateEmbedAuthor;
use serenity::framework::standard::CommandResult;
use serenity::model::channel::Message;
use serenity::prelude::*;
use serenity::utils::Color;
use serenity::framework::standard::macros::command;

#[command]
#[aliases("h")]
async fn help(ctx: &Context, msg: &Message) -> CommandResult {
    info!(
        "Use of HELP command by {}: {:?}",
        msg.author.name, msg.content
    );
    let mut author = CreateEmbedAuthor::default();
    author
        .name("Toru")
        .icon_url("https://i.redd.it/4riuxto19rq01.jpg");

    msg.channel_id.send_message(&ctx.http, |f| f.add_embed(|e| {
        e.title("Toru's Help")
        .url("https://gitlab.com/mmilek/toru")
        .author(|a| a.name("Toru").icon_url("https://i.redd.it/4riuxto19rq01.jpg") ).
        description("I'm a bot made for the Nisza server! I mainly post pictures of anime girls, but I can post from any subreddit too!")
        .fields(vec![
            ("\\r   \\reddit", "Post an image from the given subreddit. Example: '\\r emacs'", false),
            ("\\gal", "Post an anime girl from the r/awwnime subreddit. Example: '\\gal'", false),
            ("\\ra   \\redditany", "Post any type of content from the given subreddit. Example: \\ra news", false),
            ("\\h   \\help", "Post this embed", false),
            ("What else can I do?", "Besides posting daily anime girls? Not much, but feel free to submit any suggestions to the author! Link to the git repository: https://gitlab.com/mmilek/toru", false)
        ])
        .color(Color::from_rgb(250, 206, 131)).footer(|f| f.text("Have fun!"))
    })).await?;

    Ok(())
}
