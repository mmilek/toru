use crate::posts::RedditPost;
use crate::{CONFIG, PREFIX};
use chrono::{Local, Timelike};
use log::{info, warn};
use rand::Rng;
use reqwest::Url;
use serenity::framework::standard::macros::command;
use serenity::framework::standard::CommandResult;
use serenity::http::CacheHttp;
use serenity::model::channel::ReactionType;
use serenity::{client::Context, model::channel::Message};

macro_rules! fetch_url {
    ($e:expr, $msg:tt, $ctx:expr) => {
        match reqwest::get($e.clone()).await {
            Ok(r) => r,
            Err(e) => {
                $msg.reply(
                    &$ctx.http,
                    format!("Uhh, it looks like fetching the subreddit has failed... {e}"),
                )
                .await?;
                warn!("Failed to fetch subreddit. {e}");
                return Ok(());
            }
        }
    };
}

macro_rules! parse_url {
    ($u:expr, $msg:expr, $ctx:expr) => {
        match Url::parse(&$u) {
            Ok(url) => url,
            Err(e) => {
                $msg.reply(
                    &$ctx.http,
                    format!("Uhh, I don't know how to parse that into a URL...\n{e}"),
                )
                .await?;
                warn!("Failed to parse url. {e}");
                return Ok(());
            }
        }
    };
}

macro_rules! subreddit_name {
    ($msg:expr, $ctx:expr) => {
        match $msg.content.split_ascii_whitespace().nth(1) {
        Some(s) => s,
            None => {
            $msg.reply(
                &$ctx.http,
                r"I don't think you've given me a subreddit name to post from. Try using the command like this: '\post SUBREDDIT'.")
            .await?;
            warn!(
                "Invalid use of command POST by {}: {:?}",
                $msg.author.name, $msg.content
            );
            return Ok(());
        }
    }
    };
}

macro_rules! handle_null_subreddit {
    ($msg:expr, $ctx:expr, $data:expr) => {
        if $data["data"]["children"].as_array().unwrap().len() <= 0 {
            $msg.reply(&$ctx.http, "Hmm, this subreddit doesn't exist!")
                .await?;
            warn!(
                "Requested data from a non-existent subreddit: {}",
                &$msg.content
            );
            return Ok(());
        }
    };
}

macro_rules! react {
    ($msg:expr, $ctx:expr, $e:expr) => {
        $msg.react(&$ctx, ReactionType::Unicode($e.to_string()))
            .await?;
    };
}

fn get_data_len(d: &serde_json::Value) -> usize {
    d["data"]["children"]
        .as_array()
        .expect("Get data as array")
        .len()
}

fn rand_in_range(a: usize, b: usize) -> usize {
    rand::thread_rng().gen_range(a..b)
}

/// Post a random image from the first page of a subreddit
#[command]
#[aliases("r", "redditany", "ra", "gal")]
pub async fn reddit(ctx: &Context, msg: &Message) -> CommandResult {
    info!(
        "Use of REDDIT command by {}: {:?}",
        msg.author.name, msg.content
    );

    ctx.http().broadcast_typing(msg.channel_id.0).await?;

    let subreddit = if msg.content.starts_with(&format!("{PREFIX}gal")) {
        "awwnime"
    } else {
        subreddit_name!(msg, ctx)
    };

    let url_str = format!("https://reddit.com/r/{subreddit}/top.json");

    let url = parse_url!(url_str, msg, ctx);
    react!(msg, ctx, "💖");
    info!("Requesting JSON data from {url}");
    let res = fetch_url!(url.clone(), msg, ctx);
    let data: serde_json::Value =
        serde_json::from_str(&res.text().await?).expect("Parse received JSON text");
    info!("{url} data is ready");
    let mut posts = vec![];

    if data["error"] == 404 {
        msg.reply(&ctx.http, "This subreddit doesn't exist... Shame!")
            .await?;
        return Ok(());
    }
    
    if data["reason"] == "private" {
        warn!("Requested data from private subreddit: {subreddit}");
        msg.reply(&ctx.http, "This subreddit is private!")
            .await?;
        return Ok(());
    }

    let data_len = get_data_len(&data);

    if !msg.content.starts_with(&(PREFIX.to_owned() + "gal")) {
        handle_null_subreddit!(msg, ctx, data);
    }

    for idx in 0..data_len - 1 {
        let post = RedditPost::from(&data["data"]["children"][idx]["data"]);
        posts.push(post);
    }

    if !msg.content.starts_with(&(PREFIX.to_owned() + "redditany"))
        && !msg.content.starts_with(&(PREFIX.to_owned() + "ra"))
    {
        posts.retain(|p| p.is_image() /* && !p.is_imgur() */);
    }

    let mut min = 3;
    if posts.len() < min {
        min = 0
    }

    if posts.is_empty() {
        warn!("Fetched and filtered data turned out to be empty. Aborting.");
        msg.reply(&ctx.http, "Sorry, I couldn't find any fitting posts :(")
            .await?;
        return Ok(());
    }

    let rand = rand_in_range(min, posts.len());
    info!(
        "Posting {:?} to #{} in {}",
        posts[rand].url(),
        msg.channel_id.name(&ctx.cache).await.unwrap(),
        msg.guild(&ctx.cache).unwrap().name
    );
    msg.reply(ctx, posts[rand].url().to_string()).await?;

    Ok(())
}

pub async fn daily_post(ctx: &Context) -> CommandResult {
    //info!("Checking daily posting");
    if Local::now()
        .time()
        .with_second(0)
        .unwrap()
        .with_nanosecond(0)
        .unwrap()
        != CONFIG.daily_post_hour.with_second(0).unwrap()
    {
        return Ok(());
    }

    info!("Making the daily post");
    ctx.http()
        .broadcast_typing(CONFIG.daily_post_channel.0)
        .await?;
    let subreddit = "awwnime";
    let url_str = format!("https://reddit.com/r/{subreddit}/top.json");
    let url = Url::parse(&url_str).expect("Parse URL");
    info!("Requesting JSON data from {url}");

    let res = match reqwest::get(url.clone()).await {
        Ok(r) => r,
        Err(e) => {
            CONFIG
                .daily_post_channel
                .say(
                    &ctx.http,
                    format!("Uhh, it looks like fetching the subreddit has failed... {e}"),
                )
                .await?;
            warn!("Failed to fetch subreddit. {e}");
            return Ok(());
        }
    };

    let data: serde_json::Value =
        serde_json::from_str(&res.text().await?).expect("Parse received JSON text");
    info!("{url} data is ready");
    let mut posts = vec![];

    if data["error"] == 404 {
        CONFIG
            .daily_post_channel
            .say(
                &ctx.http,
                "The awwnime subreddit no longer exists... Shame!",
            )
            .await?;
        return Ok(());
    }

    let data_len = get_data_len(&data);

    for idx in 0..data_len - 1 {
        let post = RedditPost::from(&data["data"]["children"][idx]["data"]);
        posts.push(post);
    }
    posts.retain(|p| p.is_image() /* && !p.is_imgur() */);

    if posts.is_empty() {
        warn!("Fetched and filtered data turned out to be empty. Aborting.");
        CONFIG
            .daily_post_channel
            .say(
                &ctx.http,
                "Sorry, I couldn't find any fitting posts today :(",
            )
            .await?;
        return Ok(());
    }
    let daily = 3;
    let min = std::cmp::min(posts.len(), daily);

    for post in posts.iter().take(min) {
        info!(
            "Posting {:?} to #{}",
            post.url(),
            CONFIG.daily_post_channel.name(&ctx.cache).await.unwrap(),
        );
        CONFIG
            .daily_post_channel
            .say(ctx, post.url().to_string())
            .await?;
    }

    Ok(())
}
